# mediawiki-tally-counter
A userspace gadget (or a full blown extension) to enable a easy-to-use tally counter widget on any MediaWiki sites.

# Installation
1. Save the content of template.mediawiki as a template
1. Somehow install and activate main.js as a user script on the site
1. Edit the JS file's BASE_TEMPLATE_NAME to the title of the template installed at step 1
1. You're ready to go!

# Usage
See TemplateData of template.mediawiki for instructions.
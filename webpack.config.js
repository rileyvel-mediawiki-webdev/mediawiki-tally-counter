const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: {
                    loader: "ts-loader"
                }
            }
        ]
    },
    mode: "production",
    resolve: {
        extensions: ['.ts', '.js']
    },
    // optimization: {
    //     minimizer: [new TerserPlugin({
    //         terserOptions: {
    //             compress: {
    //                 passes: 999,
    //                 unsafe: true
    //             }
    //         }
    //     })]
    // }
};
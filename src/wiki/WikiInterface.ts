import { SimpleMWApi } from "simple-mw-api";

declare const mw: any;

export class WikiInterface {

    static async readCounter(counterName: string, endpoint: SimpleMWApi = new SimpleMWApi()): Promise<string> {
        try {
            return await endpoint.readPage(this.getPageNameForCounter(counterName));
        } catch (e) {
            // ignore errors and return 0
            return '0';
        }
    }


    static async writeCounter(counterName: string, value: string, summary: string, endpoint: SimpleMWApi = new SimpleMWApi()) {
        return await endpoint.writePage(this.getPageNameForCounter(counterName), value, summary);
    }

    static async purgeCurrentPage(): Promise<void> {

        const currentArticleId = mw.config.get('wgArticleId');

        const api = new mw.Api();
        await api.post({
            "action": "purge",
            "format": "json",
            "forcelinkupdate": 1,
            "pageids": `${currentArticleId}`
        });

    }

    static getPageNameForCounter(counterName: string): string {
        return `Counter:${counterName}`;
    }

}

// This class monitors the DOM site for changes, and re-call initialiseAll if needed.

import { DebouncedMutationObserver } from 'debounced-mutation-observer';
import {TallyCounterSiteFinder} from "./TallyCounterSiteFinder";

export class DocumentMonitor {

    observer = new DebouncedMutationObserver(this.callback.bind(this), 1000);

    start() {
        this.observer.observe(document.getElementById('mw-content-text')!);
    }

    callback() {

        // this is called when the observer fires
        const uninitialisedInstances = TallyCounterSiteFinder.getUninitialisedInstances();
        if (uninitialisedInstances.length > 0) {
            TallyCounterSiteFinder.initialiseAll();
        }

    }

}

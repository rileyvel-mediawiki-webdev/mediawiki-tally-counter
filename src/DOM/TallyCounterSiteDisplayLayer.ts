import {HoldableButton} from "./HoldableButton";
import {TextDisplay} from "./TextDisplay";

export class TallyCounterSiteDisplayLayer {

    // sub-elements
    minusMinusButton!: HTMLElement;
    minusButton!: HoldableButton;
    textDisplay!: TextDisplay;
    nameDisplay!: HTMLElement;
    plusButton!: HoldableButton;
    plusPlusButton!: HTMLElement;

    constructor(public wrapperElement: HTMLElement,
                public addOneCallback: VoidFunction,
                public addManyCallback: VoidFunction,
                public minusOneCallback: VoidFunction,
                public minusManyCallback: VoidFunction) {

        this._findSubElements(wrapperElement);
        this._registerElementListeners();

        if (userAgentIsDesktop()) {
            this._hideDoubleSignButtons();
        }

    }

    private _registerElementListeners() {
        this.minusMinusButton.onclick = this.minusManyCallback;
        this.minusButton.onClick = this.minusOneCallback;
        this.minusButton.onHold = this.minusManyCallback;
        this.plusButton.onClick = this.addOneCallback;
        this.plusButton.onHold = this.addManyCallback;
        this.plusPlusButton.onclick = this.addManyCallback;
    }

    private _findSubElements(wrapperElement: HTMLElement) {
        this.minusMinusButton = wrapperElement.querySelector('.tc-minus-minus') as HTMLElement;
        this.minusButton = new HoldableButton(wrapperElement.querySelector('.tc-minus') as HTMLElement);
        this.textDisplay = new TextDisplay(wrapperElement.querySelector('.tc-text') as HTMLElement);
        this.nameDisplay = wrapperElement.querySelector('.tc-name') as HTMLElement;
        this.plusButton = new HoldableButton(wrapperElement.querySelector('.tc-plus') as HTMLElement);
        this.plusPlusButton = wrapperElement.querySelector('.tc-plus-plus') as HTMLElement;
    }

    private _hideDoubleSignButtons() {
        this.minusMinusButton.style.display = 'none';
        this.plusPlusButton.style.display = 'none';
    }

    get tooltip(): string {
        return this.wrapperElement.title;
    }

    set tooltip(value: string) {
        this.wrapperElement.title = value;
    }


}

function userAgentIsDesktop() {
    return !navigator.userAgent.toLowerCase().includes('mobile');
}

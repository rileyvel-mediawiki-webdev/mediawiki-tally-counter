// Each of these represent a site in the DOM where a counter resides

import { Mainspring } from 'mainspring-debounce';
import { TextDisplay } from "./TextDisplay";
import { SweetAlertDriver } from "../SweetAlertDriver";
import { TallyCounterSiteDisplayLayer } from "./TallyCounterSiteDisplayLayer";
import { WikiInterface } from "../wiki/WikiInterface";
import { TallyCounterSiteFinder } from './TallyCounterSiteFinder';

const STD_DELAY = 1000;

declare const mw: any;

export class TallyCounterSiteController {

    private displayLayer: TallyCounterSiteDisplayLayer;
    private textDisplay: TextDisplay;
    counterName: string;

    // internal states
    private value!: number;
    private _pendingValue!: number;
    private _later: Mainspring;

    constructor(public wrapperElement: HTMLElement) {

        this.displayLayer = new TallyCounterSiteDisplayLayer(
            wrapperElement,
            this.addOne,
            this.addMultiple,
            this.minusOne,
            this.minusMultiple
        );
        this.textDisplay = this.displayLayer.textDisplay;
        this.textDisplay.addAltClickCallback(this.openCounterPageInNewTab);
        this._later = new Mainspring({ callback: this.applyValue, delay: STD_DELAY });

        this.counterName = this.displayLayer.nameDisplay.innerText;

        this.load().then(() => {
            TallyCounterSiteFinder.markAsInitialised(wrapperElement);
        });

    }

    get pendingValue(): number {
        return this._pendingValue;
    }

    /**
     * Sets the pending value, with side effects:
     * Also sets a timer to apply the new value.
     * @param newValue - value to set to
     */
    set pendingValue(newValue: number) {

        this._pendingValue = newValue;

        this.textDisplay.innerText = newValue.toString();
        this.textDisplay.isGrey = true;

        this._later.arm();

    }

    // Load the value from Wikitext.
    async load(): Promise<void> {

        this.textDisplay.innerText = 'Loading...';
        this.textDisplay.isGrey = true;

        const valueReadout = parseInt(
            await WikiInterface.readCounter(this.counterName)
        );

        this.value = valueReadout;
        this._pendingValue = valueReadout;
        this.textDisplay.innerText = valueReadout.toString();
        this.textDisplay.isGrey = false;

        this.displayLayer.tooltip = `Global tally counter. Name: ${this.counterName}`;

    }

    addOne = () => {
        this.pendingValue += 1;
    }

    minusOne = () => {
        this.pendingValue -= 1;
    }

    // will prompt the user for how many to add
    addMultiple = async () => {
        try {
            const msg = 'How many to add?';
            const value = await SweetAlertDriver.askForNumber(msg);
            this.pendingValue += value;
        } catch (e: any) {
            await SweetAlertDriver.error(e.message);
        }
    }

    // will prompt the user for how many to add
    minusMultiple = async () => {
        try {
            const msg = 'How many to subtract?';
            const value = await SweetAlertDriver.askForNumber(msg);
            this.pendingValue -= value;
        } catch (e: any) {
            await SweetAlertDriver.error(e.message);
        }
    }

    /**
     * Apply the pending value to the wikitext.
     */
    applyValue = async () => {

        try {

            const oldValue = this.value;
            const newValue = this.pendingValue;

            await WikiInterface.writeCounter(this.counterName, newValue.toString(), getSummary(oldValue, newValue));
            this.value = newValue;
            await WikiInterface.purgeCurrentPage();

            mw.notify('Your edit has been saved.');
            this.textDisplay.innerText = newValue.toString();
            this.textDisplay.isGrey = false;

        } catch (e: any) {

            SweetAlertDriver.error(`Cannot save edits: ${e.message}`);

        }

    }

    private openCounterPageInNewTab = () => {
        const pageName = WikiInterface.getPageNameForCounter(this.counterName);
        const url = getWikipageUrl(pageName);
        window.open(url, '_blank', 'noopener');
    };

}

/**
 * Renders a human-friendly difference string
 * e.g. +5 or -21
 * ASSUMES the value is not 0
 * @param oldValue
 * @param newValue
 */
function renderDifference(oldValue: number, newValue: number) {
    const d = newValue - oldValue;
    if (d > 0) {
        return '+' + d.toString();
    } else {
        return d.toString();
    }
}

function getSummary(oldValue: number, newValue: number) {
    const currentPageName = mw.config.get('wgPageName');
    const dif = renderDifference(oldValue, newValue);
    return `[assisted] Set value from ${oldValue} to ${newValue} (${dif}) from page [[${currentPageName}]]`;
}

function getWikipageUrl(pageName: string) {
    const articlePath: string = mw.config.get('wgArticlePath');
    return articlePath.replace('$1', encodeURIComponent(pageName));
}

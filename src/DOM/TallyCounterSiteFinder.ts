// This class finds sites and create their controllers.

import {TallyCounterSiteController} from "./TallyCounterSiteController";

export class TallyCounterSiteFinder {

    /**
     * Finds all sites from the DOM
     */
    static initialiseAll(): TallyCounterSiteController[] {
        const elements = TallyCounterSiteFinder.getUninitialisedInstances();
        return elements.map(item => new TallyCounterSiteController(item));
    }

    static getUninitialisedInstances() {
        return [...document.querySelectorAll('.tc-instance.tc-instance-uninitialised')] as HTMLElement[];
    }

    // Mark an instance as initialised, so it's no longer picked up by getUninitialisedInstances()
    // It's a no-op if the element is already marked
    static markAsInitialised(instance: HTMLElement) {
        instance.classList.remove('tc-instance-uninitialised');
    }

}

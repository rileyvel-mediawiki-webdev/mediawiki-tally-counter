// abstract class for the text display
// manages the appearance

export class TextDisplay {

    constructor(public element: HTMLElement) {
    }

    // isGrey controls the display colour of the text
    private _isGrey: boolean = false;

    get isGrey(): boolean {
        return this._isGrey;
    }

    set isGrey(newValue: boolean) {

        this.element.style.color = newValue ? 'grey': '';
        this._isGrey = newValue;

    }

    // innerText passes through to the element
    get innerText(): string {
        return this.element.innerText;
    }

    set innerText(newText) {
        this.element.innerText = newText;
    }

    addAltClickCallback(callback: () => any) {
        this.element.addEventListener('click', e => {
            if (e.altKey) {
                callback();
            }
        });
    }

}

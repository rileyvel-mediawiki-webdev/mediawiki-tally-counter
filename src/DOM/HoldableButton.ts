// SO button class
// to abstract away the long-press features

export class HoldableButton {

    // inputs
    onClick = () => {};
    onHold = () => {};

    private lastMouseDownT = 0;

    constructor(public element: HTMLElement) {
        const mouseDown = this.handleMouseDown.bind(this);
        const mouseUp = this.handleMouseUp.bind(this);
        element.addEventListener('mousedown', mouseDown);
        element.addEventListener('mouseup', mouseUp);
    }

    handleMouseDown() {
        this.lastMouseDownT = Date.now();
    }

    handleMouseUp(event: MouseEvent) {

        // if using shift, trigger onHold instantly
        if (event.shiftKey) {
            this.onHold();
            return;
        }

        const delay = Date.now() - this.lastMouseDownT;

        if (delay > STD_DELAY) {
            this.onHold();
        } else{
            this.onClick();
        }

    }

}

// standard long-press delay, in ms.
const STD_DELAY = 500;

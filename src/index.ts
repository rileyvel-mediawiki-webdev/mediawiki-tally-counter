// Start Coding Here

import { TallyCounterGadget } from "./TallyCounterGadget";
import { DocumentMonitor } from "./DOM/DocumentMonitor";

declare const mw: any;

function eligible(): boolean {
    return mw.config.get('wgIsArticle');
}

function init() {
    if (eligible()) {
        new TallyCounterGadget();
        new DocumentMonitor().start();
    }
}

// @ts-ignore
requestIdleCallback(init);

// Main class for the gadget

import {TallyCounterSiteFinder} from "./DOM/TallyCounterSiteFinder";

export class TallyCounterGadget {

    constructor() {
        TallyCounterSiteFinder.initialiseAll();
    }
    
}

// This is a dialogue to ask the user for a number input.

import Sweetalert2 from 'sweetalert2';

const Toast = Sweetalert2.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Sweetalert2.stopTimer)
        toast.addEventListener('mouseleave', Sweetalert2.resumeTimer)
    }
})


export class SweetAlertDriver {

    static async askForNumber(msg: string): Promise<number> {

        const input = await Sweetalert2.fire({
            title: 'What is your number?',
            text: msg,
            input: 'number'
        });

        const answer = parseInt(input.value);
        if (isNaN(answer)) {
            throw new Error('That doesn\'t look like a number.');
        }
        return answer;

    }

    static async success(msg: string): Promise<void> {
        await Toast.fire({
            icon: 'success',
            title: 'Success',
            text: msg,
        });
    }

    static async error(msg: string): Promise<void> {
        await Sweetalert2.fire('Error', msg, 'error');
    }

}